# duderto-bot

A Telegram Bot with a violent sounding name.

## Development

Steps to obtain a working development setup:

- `python3 -m venv venv`  
- `. venv/bin/activate`  
- `pip install wheel`  
- `pip install -r requirements-dev.txt`

## Production

Steps to obtain a working production setup:

- `python3 -m venv venv`  
- `. venv/bin/activate`  
- `pip install wheel`  
- `pip install -r requirements.txt`  
- `deactivate`
