#!/usr/bin/env python3

import argparse
import functools
import logging
import re
import sys
from telegram import Update
from telegram.ext import Updater, MessageHandler, Filters, CallbackContext
from telegram.utils.helpers import mention_html
import toml

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


def reply_callback(update: Update, context: CallbackContext, user_id: int):
    chat_member = context.bot.get_chat_member(update.message.chat_id, user_id)
    # if user_id was not matched in the current chat, the function will exit

    # tag by username or mention first name
    if chat_member.user.username:
        reply = '@{}'.format(chat_member.user.username)
    else:
        reply = mention_html(chat_member.user.id, chat_member.user.first_name)

    logger.info('Match "%s" - Tagging %s', context.match.group(0), reply)
    update.message.reply_html(reply)


def error_callback(update, context):
    logger.warning('Update "%s" - Error "%s"', update, context.error)


def main():
    # initialize parser
    parser = argparse.ArgumentParser(
        description='A Telegram Bot with a violent sounding name')

    # add required arguments
    parser.add_argument('-f', '--config-file', type=str, metavar='CONFIG',
                        required=True,
                        help='configuration file')

    # parse command line arguments
    args = parser.parse_args()

    # load configuration file
    with open(args.config_file) as f:
        config = toml.load(f)

    # verify configuration parameters
    if 'key' not in config:
        logger.error('Configuration for `key` missing')
        sys.exit(1)
    if 'entry' not in config:
        logger.error('Configuration for `entry` missing')
        sys.exit(1)

    api_key = config['key']
    entries = config['entry']

    # initialize Telegram updater
    updater = Updater(api_key, use_context=True)
    updater.dispatcher.add_error_handler(error_callback)

    for entry in entries:
        # verify configuration parameters
        if 'match' not in entry:
            logger.error('Configuration for `entry.match` missing')
            sys.exit(1)
        if 'user_id' not in entry:
            logger.error('Configuration for `entry.user_id` missing')
            sys.exit(1)

        # compile RegExp
        re_match = re.compile(entry['match'], re.IGNORECASE)

        # define callback functions with user_id argument
        reply_callback_for_entry = functools.partial(
            reply_callback, user_id=entry['user_id'])

        # initialize match-reply handler for entry
        updater.dispatcher.add_handler(
            MessageHandler(Filters.regex(re_match), reply_callback_for_entry))

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
